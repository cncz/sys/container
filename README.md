# pgo-caddy

Caddy configuration for [pgo](github.com/miekg/pgo).
Runs on containervm{12}.

pgo config:

~~~ toml
[[services]]
name = "default"
user = "root"
repository = "https://gitlab.science.ru.nl/cncz/sys/container"
import = "caddy/Caddyfile-import"
~~~
